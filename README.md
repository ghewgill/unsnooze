# Unsnooze

`unsnooze` is a simple script that implements email "snooze" functionality for Maildir format mailboxes.

## Usage

`unsnooze` takes a single argument which is the name of a Maildir mailbox folder.
It is normally called from `cron` at certain times of the day and week (see below).

## Operation

When called with an argument that is not `Someday`, then unsnooze moves all the mail files from the named folder to the inbox.

When called with `Someday` as an argument, then unsnooze selects each mail file in the Someday folder with a 1% probability to move to the inbox.

## Configuration

Create a top level folder named `Snooze`.
Create sub-folders for `Tomorrow`, `Next-week`, and `Someday`.

Set up your `crontab` as follows (times may need adjusting for your time zone or preferences):

    0 6 * * *	unsnooze Tomorrow
    0 6 * * 1	unsnooze Next-week
    0 6 * * *	unsnooze Someday
